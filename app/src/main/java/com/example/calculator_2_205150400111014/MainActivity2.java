package com.example.calculator_2_205150400111014;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity2 extends AppCompatActivity {
    TextView hasil;
    Button kembali;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        int v1 = bundle.getInt("v1");
        int v2 = bundle.getInt("v2");
        int v3 = bundle.getInt("v3");
        char act = bundle.getChar("act");

        hasil = (TextView) findViewById(R.id.hasil);
        kembali = (Button) findViewById(R.id.kembali);

        hasil.setText(v1 + " " + act + " " + v2 + " = " + v3);

        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }
}