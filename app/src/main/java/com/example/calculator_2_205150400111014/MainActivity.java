package com.example.calculator_2_205150400111014;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button b0, b1, b2, b3, b4, b5, b6,
            b7, b8, b9, badd, bsub, bdiv,
            bmul, bC, beq;

    int mValueOne, mValueTwo;

    EditText input;

    boolean isAddition, isSubtract, isMultiplication, isDivision;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b0 = (Button) findViewById(R.id.b0);
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        b3 = (Button) findViewById(R.id.b3);
        b4 = (Button) findViewById(R.id.b4);
        b5 = (Button) findViewById(R.id.b5);
        b6 = (Button) findViewById(R.id.b6);
        b7 = (Button) findViewById(R.id.b7);
        b8 = (Button) findViewById(R.id.b8);
        b9 = (Button) findViewById(R.id.b9);
        badd = (Button) findViewById(R.id.badd);
        bsub = (Button) findViewById(R.id.bsub);
        bmul = (Button) findViewById(R.id.bmul);
        bdiv = (Button) findViewById(R.id.bdiv);
        bC = (Button) findViewById(R.id.bC);
        beq = (Button) findViewById(R.id.beq);
        input = (EditText) findViewById(R.id.input);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "1");
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "2");
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "3");
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "4");
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "5");
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "6");
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "7");
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "8");
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "9");
            }
        });
        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText(input.getText() + "0");
            }
        });
        badd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (input == null) {
                    input.setText("");
                } else {
                    mValueOne = Integer.parseInt(input.getText() + "");
                    isAddition = true;
                    input.setText(null);
                }
            }
        });
        bsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Integer.parseInt(input.getText() + "");
                isSubtract = true;
                input.setText(null);
            }
        });
        bmul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Integer.parseInt(input.getText() + "");
                isMultiplication = true;
                input.setText(null);
            }
        });
        bdiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueOne = Integer.parseInt(input.getText() + "");
                isDivision = true;
                input.setText(null);
            }
        });
        beq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mValueTwo = Integer.parseInt(input.getText() + "");
                Intent intent = new Intent(v.getContext(), MainActivity2.class);
                Bundle b = new Bundle();
                b.putInt("v1", mValueOne);
                b.putInt("v2", mValueTwo);
                if (isAddition == true) {
                    b.putChar("act", '+');
                    b.putInt("v3", mValueOne+mValueTwo);
                    isAddition = false;
                }
                if (isSubtract == true) {
                    b.putChar("act", '-');
                    b.putInt("v3", mValueOne-mValueTwo);
                    isSubtract = false;
                }
                if (isMultiplication == true) {
                    b.putChar("act", '*');
                    b.putInt("v3", mValueOne*mValueTwo);
                    isMultiplication = false;
                }
                if (isDivision == true) {
                    b.putChar("act", '/');
                    b.putInt("v3", mValueOne/mValueTwo);
                    isDivision = false;
                }
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        bC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input.setText("");
            }
        });
    }
}